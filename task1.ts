const axios = require('axios').default;
const yargs = require('yargs');

class Character {
    id: number;
    name: string;
    status: string;
    gender: string;

    constructor (id: number, name: string, status: string, gender: string) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.gender = gender;
    }

}
let characters: Character[] = [];


function push_data(results: any[]) {
    if (results) {
        for (let result of results) {
            characters.push(new Character(result.id, result.name, result.status, result.gender));
        }
    }
}

async function get_data(url: string) {
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
        console.error(error);
    }
}

async function main() {
    console.log('Loading...');

    let url: any = 'https://rickandmortyapi.com/api/character';

    for (let i = 0; i < 1; i++) {
        let response = await get_data(url)
            .then(function(result) {
                url = result.info.next;             
                push_data(result.results)
            });
        
        if (url != null && url.indexOf('https') > -1) i -= 1;
    } 

    let uniqStatuses: string[] = [...new Set(characters.map(item => item.status))];
    let uniqGenders: string[] = [...new Set(characters.map(item => item.gender))];
    let random = Math.floor(Math.random() * characters.length);

    const argv = yargs.options({
        id: {type: 'number', describe: 'example: ' + characters[random].id},
        n: {alias: 'name', type: 'string', describe: 'example: ' + characters[random].name},
        g: {alias: 'gender', choices: uniqGenders, describe: 'example: ' + characters[random].gender},
        s: {alias: 'status', choices: uniqStatuses, describe: 'example: ' + characters[random].status}
    }).argv;

    if (argv.id) characters = characters.filter((v) => v.id == argv.id);
    if (argv.n) characters = characters.filter((v) => v.name == argv.n);
    if (argv.g) characters = characters.filter((v) => v.gender == argv.g);
    if (argv.s) characters = characters.filter((v) => v.status == argv.s);

    console.log("\n=================\n");
    
    if (characters.length > 0) console.log(characters);
    else console.log("Nothing found. Try to change request.");

    console.log("\n=================\n");

    yargs.showHelp();
}

main();