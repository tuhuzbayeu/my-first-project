"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const axios = require('axios').default;
const yargs = require('yargs');
class Character {
    constructor(id, name, status, gender) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.gender = gender;
    }
}
let characters = [];
function push_data(results) {
    if (results) {
        for (let result of results) {
            characters.push(new Character(result.id, result.name, result.status, result.gender));
        }
    }
}
function get_data(url) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const response = yield axios.get(url);
            return response.data;
        }
        catch (error) {
            console.error(error);
        }
    });
}
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        console.log('Loading...');
        let url = 'https://rickandmortyapi.com/api/character';
        for (let i = 0; i < 1; i++) {
            let response = yield get_data(url)
                .then(function (result) {
                url = result.info.next;
                push_data(result.results);
            });
            if (url != null && url.indexOf('https') > -1)
                i -= 1;
        }
        //let unique = characters.filter((v) => v.id < 10);
        let uniqStatuses = [...new Set(characters.map(item => item.status))];
        let uniqGenders = [...new Set(characters.map(item => item.gender))];
        let random = Math.floor(Math.random() * characters.length);
        const argv = yargs.options({
            id: { type: 'number', describe: 'example: ' + characters[random].id },
            n: { alias: 'name', type: 'string', describe: 'example: ' + characters[random].name },
            g: { alias: 'gender', choices: uniqGenders, describe: 'example: ' + characters[random].gender },
            s: { alias: 'status', choices: uniqStatuses, describe: 'example: ' + characters[random].status }
        }).argv;
        if (argv.id)
            characters = characters.filter((v) => v.id == argv.id);
        if (argv.n)
            characters = characters.filter((v) => v.name == argv.n);
        if (argv.g)
            characters = characters.filter((v) => v.gender == argv.g);
        if (argv.s)
            characters = characters.filter((v) => v.status == argv.s);
        console.log("\n=================\n");
        if (characters.length > 0)
            console.log(characters);
        else
            console.log("Nothing found. Try to change request.");
        console.log("\n=================\n");
        yargs.showHelp();
    });
}
main();
